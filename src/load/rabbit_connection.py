from kombu import Connection


class RabbitChanel:
    def __init__(self, broker_url):
        self.broker_url = broker_url

    def __enter__(self):
        self.conn = Connection(self.broker_url)
        self.channel = self.conn.channel()
        return self.channel

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.channel.close()
        self.conn.close()