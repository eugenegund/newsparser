from kombu import Connection, Producer
from settings import CELERY_BROKER_URL
from rabbit_connection import RabbitChanel


def send_message(mess: dict, queue: str) -> None:
    with RabbitChanel(CELERY_BROKER_URL) as channel:
        producer = Producer(channel)
        producer.publish(mess, retry=True, routing_key=queue)
