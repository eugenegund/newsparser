import requests as req
from bs4 import BeautifulSoup
import re


class PenzaNewsParser:
    url = "https://penzanews.ru/"

    def news_urls(self, url_end: str = "") -> list:
        """ If url_end = False - return first urls news. Else return the list urls news before url_end """

        news_urls = []
        resp = req.get(self.url)
        root_tag = BeautifulSoup(resp.text, "lxml")
        list_news_tag = root_tag.find("ul", attrs={"class": "NewsList"})
        news_tags = list_news_tag.find_all(
            lambda tag: tag.name == "li"
            and tag.get("class") != ["info"]
            and tag.get("class") != ["work"]
        )

        for news_tag in news_tags:
            url_news_tag = news_tag.find("a")
            href_news = f'{url_news_tag.get("href")}'
            news_urls.append(href_news)

            if not url_end or re.findall(r"[\d']+", href_news) == re.findall(
                r"[\d']+", url_end
            ):
                break

        return news_urls

    def get_news(self, url: str) -> dict:
        """ Return dict news {"title", "text", "rubric", "date"} """

        resp = req.get(self.url + url)
        root = BeautifulSoup(resp.text, "lxml")
        news_tag = root.find("div", attrs={"class": "NewsText"})

        title_tag = news_tag.find("div", attrs={"class": "title"})
        title = title_tag.contents[1].text

        date_tag = title_tag.find("a", attrs={"class": "date"})
        date_list = re.findall(r"[\d']+", date_tag.text)
        date = "{}-{}-{} ".format(*date_list[4:1:-1]) + "{}:{}".format(
            *date_list[0:2:1]
        )

        rubric_tag = title_tag.find("a", attrs={"class": "category"})
        rubric = rubric_tag.get("href").replace("/", "")

        p_tags = news_tag.find_all(
            lambda tag: tag.name == "p" and tag.get("class") != ["ImgBuy"]
        )

        btag = news_tag.find("b")
        btag.decompose()

        text = ""
        for tag in p_tags:
            text += tag.text
            text += "/n"

        return {"title": title, "text": text, "rubric": rubric, "pub_date": date}

    def receive_news(self, url_end: str = "") -> list:
        """ Return list news before url_end news """

        return [self.get_news(href_news) for href_news in self.news_urls(url_end)]


if __name__ == "__main__":
    parser = PenzaNewsParser()
    print(parser.receive_news("142883-2020"))
