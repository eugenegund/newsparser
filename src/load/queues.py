from __future__ import absolute_import, unicode_literals
from kombu import Exchange, Queue

penzanews_queue = Queue("penzanews", Exchange("penzanews"), routing_key="penzanews")
news_queue = Queue("news", Exchange("news"), routing_key="news")
