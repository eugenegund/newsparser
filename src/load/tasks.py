import json
from celery import Celery, bootsteps
from kombu import Consumer
from parser_penzanews import PenzaNewsParser
from rabbit import send_message
from settings import CELERY_BROKER_URL
import queues

app = Celery("tasks", broker=CELERY_BROKER_URL)


class PenzaNewsConsumerStep(bootsteps.ConsumerStep):
    def get_consumers(self, channel):
        return [
            Consumer(
                channel,
                queues=[queues.penzanews_queue],
                callbacks=[self.start_parser_task],
                accept=["json"],
            )
        ]

    def start_parser_task(self, body, message):
        print("Received message: {0!r}".format(body))
        href_news = json.loads(message.body).get("href")
        send_news_task.delay(href_news)
        message.ack()


app.steps["consumer"].add(PenzaNewsConsumerStep)


@app.task
def send_news_task(href_news):
    print(f"SEND MESSAGE: {href_news}")
    parser = PenzaNewsParser()
    news = parser.receive_news(href_news)
    for obj in news:
        send_message(obj, queues.news_queue.name)
